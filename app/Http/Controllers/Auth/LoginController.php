<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Http\Requests\Login\CreateLoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(CreateLoginRequest $request)
    {
        $user = User::where(['email' =>  $request->email])->first();
        if (!empty($user) && Hash::check($request['password'], $user->password)) 
        {
            $userdata = array(
                'email'    => $request->email,
                'password' => $request->password,
                'status'   => 1,
            );
            if(Auth()->attempt($userdata))
            {
                if(Auth()->user()->role == 'admin') 
                {
                    return redirect()->route('admin.dashboard');
                }
                else
                {
                    return redirect()->route('user.dashboard');
                }
            }
            else
            {
                return redirect('login')->withInput()->with('error',trans("message.account_deactive"));
            }
        }
        else
        {
            return redirect('login')->withInput()->with('error',trans("message.credentials_not_match"));
        }
    }
}
