<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\Category\CreateCategoryRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Validator;

class CategoryController extends Controller
{
    public function index()
    {
        $pageConfigs = ['pageHeader' => true,'controller' => 'CategoryController'];
        $category = Category::OrderBy('id','desc')->get();
        return view('admin.category.index',['pageConfigs' => $pageConfigs,'category' => $category]);
    } 
    public function create()
    {
        $pageConfigs = ['pageHeader' => true,'controller' => 'CategoryController'];
        return view('admin.category.form',['pageConfigs' => $pageConfigs]);
    }
    public function store(CreateCategoryRequest $request)
    {
        try{
            $category              = new Category;
            $category->title       = $request->title;
            $category->description = $request->description;
            $category->status      = $request->status;
            $category->save();
            return redirect()->route('admin.list-category')->with('success',trans("message.add_category"));
        }
        catch(\Exception $e)
        {
            return redirect()->route('admin.list-category')->with('error',$e->getMessage());
        }
    }
    public function edit($category_id)
    {
        try{
            $category_id = decrypt($category_id);
            $category = Category::find($category_id);
            if(!empty($category))
            {
                $pageConfigs = ['pageHeader' => true,'controller' => 'CategoryController'];
                return view('admin.category.edit',['pageConfigs' =>$pageConfigs,'category' => $category]);
            }
            else
            {
                return redirect()->route('admin.list-category')->with('error',trans("message.something_wrong"));
            }
        }
        catch(\Exception $e)
        {
            return redirect()->route('admin.list-category')->with('error',$e->getMessage());
        }
    }
    public function update(Request $request)
    {
        try{
            $category_id = decrypt($request->category_id);
            $category = Category::find($category_id);
            if(!empty($category))
            {
                $validator = Validator::make($request->all(), [
                    'title'       => 'required|max:100|unique:category,title,'.$category_id, 
                    'description' => 'required|max:255',
                    'status'      => 'required|in:1,0'  
                ]);
                if($validator->fails())
                {
                    return redirect()->route('admin.edit-category',$request->category_id)->withInput()->withErrors($validator->errors());;
                }
                else
                {
                    $category->title       = $request->title;
                    $category->description = $request->description;
                    $category->status      = $request->status;
                    $category->save();

                    return redirect()->route('admin.list-category')->with('success',trans("message.update_category"));

                }
            }
            else
            {
                return redirect()->route('admin.list-category')->with('error',trans("message.something_wrong"));
            }
        }catch(\Exception $e)
        {
            return redirect()->route('admin.list-category')->with('error',$e->getMessage());
        }
    }
    public function delete($category_id)
    {
        try{
            $category_id = decrypt($category_id);
            $category = Category::find($category_id);
            if(!empty($category))
            {
                $category->delete();
                return redirect()->route('admin.list-category')->with('success',trans("message.delete_category"));
            }
            else
            {
                return redirect()->route('admin.list-category')->with('error',trans("message.something_wrong"));
            }
        }
        catch(\Exception $e)
        {
            return redirect()->route('admin.list-category')->with('error',$e->getMessage());
        }
    }
}
