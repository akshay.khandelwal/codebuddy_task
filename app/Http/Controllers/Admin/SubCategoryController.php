<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Category,SubCategory};

class SubCategoryController extends Controller
{
    public function index($category_id)
    {
        $category_id = decrypt($category_id);
        $category = Category::find($category_id);
        if(!empty($category))
        {
            $pageConfigs = ['pageHeader' => true,'controller' => 'CategoryController'];
            $subcategory = SubCategory::OrderBy('id','desc')->get();
            return view('admin.subcategory.index',['pageConfigs' => $pageConfigs,'subcategory' => $subcategory,'category' => $category]);
        }
        else
        {
            return redirect()->route('admin.list-category')->with('error',trans("message.something_wrong"));
        }
    } 
    public function store(Request $request)
    {
        try{
            $subcategory              = new SubCategory;
            $subcategory->category_id = $request->category_id;
            $subcategory->title       = $request->title;
            $subcategory->status      = $request->status;
            $subcategory->save();
            return redirect()->back()->with('success',trans("message.add_category"));
        }
        catch(\Exception $e)
        {
            return redirect()->route('admin.list-category')->with('error',$e->getMessage());
        }
    }
    public function delete($subcategory_id)
    {
        try{
            $subcategory_id = decrypt($subcategory_id);
            $subcategory = SubCategory::find($subcategory_id);
            if(!empty($subcategory))
            {
                $subcategory->delete();
                return redirect()->back()->with('success',trans("message.delete_category"));
            }
            else
            {
                return redirect()->route('admin.list-category')->with('error',trans("message.something_wrong"));
            }
        }
        catch(\Exception $e)
        {
            return redirect()->route('admin.list-category')->with('error',$e->getMessage());
        }
    }
}
