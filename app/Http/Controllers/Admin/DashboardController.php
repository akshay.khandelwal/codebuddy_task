<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{User,Category};
class DashboardController extends Controller
{
    public function index()
    {
        $pageConfigs = ['pageHeader' => true,'controller' => 'DashboardController'];
        $total_user = User::count();
        $total_category = Category::count();
        return view('admin.dashboard',['pageConfigs' => $pageConfigs,'total_user' => $total_user,'total_category' => $total_category]);
    } 
}
