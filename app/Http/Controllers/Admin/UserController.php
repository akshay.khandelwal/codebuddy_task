<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $pageConfigs = ['pageHeader' => true,'controller' => 'UserController'];
        $users = User::where('role','user')->OrderBy('id','desc')->get();
        return view('admin.users.index',['pageConfigs' => $pageConfigs,'users' => $users]);
    } 
}
