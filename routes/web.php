<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\{DashboardController,UserController,CategoryController,SubCategoryController};
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'admin','middleware'=>'is_admin'], function () {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');
    Route::get('list-users', [UserController::class, 'index'])->name('admin.list-users');
    
    Route::get('list-category', [CategoryController::class, 'index'])->name('admin.list-category');
    Route::get('add-category', [CategoryController::class, 'create'])->name('admin.add-category');
    Route::post('add-category', [CategoryController::class, 'store'])->name('admin.store-category');
    Route::get('edit-category/{id}', [CategoryController::class, 'edit'])->name('admin.edit-category');
    Route::get('delete-category/{id}', [CategoryController::class, 'delete'])->name('admin.delete-category');
    Route::post('update-category', [CategoryController::class, 'update'])->name('admin.update-category');

    Route::get('list-subcategory/{category_id}', [SubCategoryController::class, 'index'])->name('admin.list-subcategory');
    Route::post('add-subcategory', [SubCategoryController::class, 'store'])->name('admin.store-subcategory');
    Route::get('delete-subcategory/{id}', [SubCategoryController::class, 'delete'])->name('admin.delete-subcategory');
});
Route::group(['prefix' => 'user','middleware'=>'is_user'], function () {
    Route::get('dashboard', [App\Http\Controllers\User\DashboardController::class, 'index'])->name('user.dashboard');
});
