<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'          => 'Akshay Kedawat',
            'email'         => 'akshaykedawat799@gmail.com',
            'mobile_number' => '8955318149',
            'role'          => 'user',
            'status'        => 1,
            'password'      => Hash::make('Test@123')
        ]);

        User::create([
            'name'          => 'Admin',
            'email'         => 'admin@gmail.com',
            'mobile_number' => '7232047238',
            'role'          => 'admin',
            'status'        => 1,
            'password'      => Hash::make('Test@123')
        ]);

    }
}
