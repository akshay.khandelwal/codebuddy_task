<?php 
    return [
        'credentials_not_match' => 'These credentials do not match our records.',
        'something_wrong'       => 'Something wrong pls try again after some time.',
        'account_deactive'      => 'Your account is deactivated contact to administrator !',
        'add_category'          => 'Category added successfully.',
        'update_category'       => 'Category updated successfully.',
        'delete_category'       => 'Category delete successfully.'
    ]
?>