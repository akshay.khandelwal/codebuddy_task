@extends('layouts.master')
@section('title', 'Login |')
@section('content')
<div id="main-wrapper">
    <div class="">
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
            <div class="auth-box bg-dark border-secondary" style="background-color: #373737!important">     
                @include('components.message')
                <div align="center">
                    <a href="/admin/login">
                        codebuddy
                    </a>
                </div>
                <div id="loginform">
                    <form autocomplete="off" class="form-horizontal m-t-20" id="loginform" action="{{ route('login') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row p-b-30">
                            <div class="col-12">
                                <div class="mb-3"> 
                                    <input autocomplete="off" type="email" id="email" name="email" class="form-control form-control-lg" placeholder="Email" value="{{ old('email') }}" aria-label="Username" aria-describedby="basic-addon1">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="mb-3"> 
                                    <input name="password" id="password" type="password" class="form-control form-control-lg" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            {{ $errors->first('password') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row border-top border-secondary">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="p-t-20">
                                        <button id="submit-btn" class="btn btn-success float-right" type="submit"><span id="licon"></span>Login</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            

            </div>
        </div>
    </div>
</div>
@endsection
