@include('components.styles')
<div id="main-wrapper"> 
    @include('components.header')
    @include('components.sidebar')
    @yield('content')
</div>
@include('components.scripts') 