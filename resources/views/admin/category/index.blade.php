@extends('layouts.app')
@section('title', 'List Category |')
@section('content')
<div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">List Category</h4>
                    <div class="ml-auto text-right">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li> 
                                <li class="breadcrumb-item active">List Category</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div align = "right" class="mb-3"> 
                        <a href="{{route('admin.add-category')}}" class = "btn btn-success">Add</a>
                    </div>
                    @include('components.message')
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive"> 
                                <table class="table table-striped table-bordered dtable" id="users-table">
                                    <thead>
                                        <th>ID</th>    
                                        <th>Title</th>    
                                        <th>Created at</th>
                                        <th>Status</th>   
                                        <th>Action</th> 
                                    </thead> 
                                    <tbody>
                                        @php $i = 1; @endphp
                                        @foreach($category as $row)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $row->title }}</td>
                                                <td>{{ date('d-M-Y h:i a',strtotime($row->created_at)) }}</td>
                                                <td>
                                                    @if($row->status == 1)
                                                        <span class="f-left margin-r-5 1"><a href="javascript:void(0)" class="btn btn-xs btn-success" title="Active">Active</a></span>
                                                    @else 
                                                        <span class="f-left margin-r-5 1" ><a href="javascript:void(0)" class="btn btn-xs btn-danger" title="Active" >Inctive</a></span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <span class='f-left margin-r-5'>
                                                        <a data-toggle='tooltip'  class='btn btn-primary btn-xs' title='Edit' href="{{ route('admin.edit-category',encrypt($row->id)) }}">
                                                            <i class="fa fa-edit" aria-hidden='true'></i>				
                                                        </a>
                                                    </span>
                                                    <span class='f-left margin-r-5'>
                                                        <a data-toggle='tooltip' onclick="return confirm('Are you sure?')" class='btn btn-danger btn-xs' title='Delete' href="{{ route('admin.delete-category',encrypt($row->id)) }}">
                                                            <i class="fa fa-trash" aria-hidden='true'></i>				
                                                        </a>
                                                    </span>
                                                    <span class='f-left margin-r-5'>
                                                        <a data-toggle='tooltip' class='btn btn-primary btn-xs' title='SubCategory' href="{{ route('admin.list-subcategory',encrypt($row->id)) }}">
                                                            <i class="fa fa-tree" aria-hidden='true'></i>
                                                            SubCategory				
                                                        </a>
                                                    </span>
                                                </td>
                                            </tr>       
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
