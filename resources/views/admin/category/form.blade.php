@extends('layouts.app')
@section('title', 'Add Category |')
@section('content')
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">Add Category</h4>
                    <div class="ml-auto text-right">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li> 
                                <li class="breadcrumb-item active">Add Category</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body"> 
                            <div class="card-body">
                                <form class="form-horizontal" method="POST" action="{{ route('admin.store-category') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label class="">Title *</label> 
                                        <input type="text" name="title" id="title" class="form-control" placeholder="Title" value="{{ old('title') }}" maxlength="100" >
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                {{ $errors->first('title') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="">Description *</label> 
                                        <textarea class="form-control" name="description" rows="4" cols="4">{{ old('description') }}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                {{ $errors->first('description') }}
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="">Status *</label> 
                                        <select class="form-control" name="status" id="status" >
                                            <option value="">Select Status</option>
                                            <option value="1" {{ old('status')==1 ? 'selected':'' }}>Active</option>
                                            <option value="0" {{ old('status')==2 ? 'selected':'' }}>Inctive</option>
                                        </select>
                                        @if ($errors->has('status'))
                                            <span class="help-block">
                                                {{ $errors->first('status') }}
                                            </span>
                                        @endif
                                    </div>
                                    <button class="btn btn-primary" id="submit-btn" type="submit"><span id="licon"></span>Save</button> 
                                    <a class="btn btn-secondary" href="{{route('admin.list-category')}}">Back</a> 
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
