@extends('layouts.app')
@section('title', 'Dashboard |')
@section('content')
<div class="page-wrapper">
		<div class="page-breadcrumb">
			<div class="row">
				<div class="col-12 d-flex no-block align-items-center">
				<h4 class="page-title"></h4>
					<div class="ml-auto text-right">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li> 
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<h4>Dashboard</h4>
				</div>
				<div class="col-md-6 col-lg-4"> 
					<div class="card card-hover">
						<div class="box bg-cyan text-center">
							<h1 class="font-light text-white">{{$total_user}}</h1>
							<h6 class="text-white">Total Users</h6>
						</div>
					</div> 
				</div>
				<div class="col-md-6 col-lg-4"> 
					<div class="card card-hover">
						<div class="box bg-cyan text-center">
							<h1 class="font-light text-white">{{ $total_category }}	</h1>
							<h6 class="text-white">Total Category</h6>
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
@endsection
