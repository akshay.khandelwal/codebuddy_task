@extends('layouts.app')
@section('title', 'List SubCategory |')
@section('content')
<div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">List SubCategory</h4>
                    <div class="ml-auto text-right">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li> 
                                <li class="breadcrumb-item active">List SubCategory</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h5 class="page-title">Category :- {{$category->title}}</h5>
                    <div align = "right" class="mb-3"> 
                        <button type="button" data-toggle="modal" data-target="#myModal" class = "btn btn-success">Add</button>
                    </div>
                    @include('components.message')
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive"> 
                                <table class="table table-striped table-bordered dtable" id="users-table">
                                    <thead>
                                        <th>ID</th>    
                                        <th>Title</th>    
                                        <th>Created at</th>
                                        <th>Status</th>  
                                        <th>Action</th> 
                                    </thead> 
                                    <tbody>
                                        @php $i = 1; @endphp
                                        @foreach($subcategory as $row)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $row->title }}</td>
                                                <td>{{ date('d-M-Y h:i a',strtotime($row->created_at)) }}</td>
                                                <td>
                                                    @if($row->status == 1)
                                                        <span class="f-left margin-r-5 1"><a href="javascript:void(0)" class="btn btn-xs btn-success" title="Active">Active</a></span>
                                                    @else 
                                                        <span class="f-left margin-r-5 1" ><a href="javascript:void(0)" class="btn btn-xs btn-danger" title="Active" >Inctive</a></span>
                                                    @endif
                                                </td>
                                                <td>    
                                                    <span class='f-left margin-r-5'>
                                                        <a data-toggle='tooltip' onclick="return confirm('Are you sure?')" class='btn btn-danger btn-xs' title='Delete' href="{{ route('admin.delete-subcategory',encrypt($row->id)) }}">
                                                            <i class="fa fa-trash" aria-hidden='true'></i>				
                                                        </a>
                                                    </span>
                                                </td>
                                            </tr>       
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form class="form-horizontal" method="POST" action="{{ route('admin.store-subcategory') }}">
                    @csrf
                    <input type="hidden" name="category_id" id="category_id" class="form-control" placeholder="Title" value="{{ $category->id }}" maxlength="100" required>
                    <div class="form-group">
                        <label class="">Title *</label> 
                        <input type="text" name="title" id="title" class="form-control" placeholder="Title" value="{{ old('title') }}" maxlength="100" required>
                    </div>
                    <div class="form-group">
                        <label class="">Status *</label> 
                        <select class="form-control" name="status" id="status" required>
                            <option value="">Select Status</option>
                            <option value="1">Active</option>
                            <option value="0">Inctive</option>
                        </select>
                    </div>
                    <button class="btn btn-primary" id="submit-btn" type="submit"><span id="licon"></span>Save</button> 
                </form> 
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection