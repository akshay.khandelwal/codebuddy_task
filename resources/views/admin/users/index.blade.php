@extends('layouts.app')
@section('title', 'List Users |')
@section('content')
<div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">List Users</h4>
                    <div class="ml-auto text-right">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li> 
                                <li class="breadcrumb-item active">List Users</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive"> 
                                <table class="table table-striped table-bordered dtable" id="users-table">
                                    <thead>
                                        <th>ID</th>    
                                        <th>Name</th>    
                                        <th>Email</th>   
                                        <th>Mobile Number</th>
                                        <th>Created at</th>
                                        <th>Status</th>    
                                    </thead> 
                                    <tbody>
                                        @php $i = 1; @endphp
                                        @foreach($users as $row)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $row->name }}</td>
                                                <td>{{ $row->email }}</td>
                                                <td>{{ $row->mobile_number }}</td>
                                                <td>{{ date('d-M-Y h:i a',strtotime($row->created_at)) }}</td>
                                                <td>
                                                    @if($row->status == 1)
                                                        <span class="f-left margin-r-5 1"><a href="javascript:void(0)" class="btn btn-xs btn-success" title="Active">Active</a></span>
                                                    @else 
                                                        <span class="f-left margin-r-5 1" ><a href="javascript:void(0)" class="btn btn-xs btn-success" title="Active" >Inctive</a></span>
                                                    @endif
                                                </td>
                                            </tr>       
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
