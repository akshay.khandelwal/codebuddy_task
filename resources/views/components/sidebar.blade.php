@php
	$controller = isset($pageConfigs['controller'])?$pageConfigs['controller']:'';
@endphp
<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">
                @php
                    $active = ($controller == 'DashboardController') ? 'selected' : '';
                @endphp
                <li class="sidebar-item {{$active}}"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link active" href="{{ route('admin.dashboard') }}" aria-expanded="false">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                @php
                    $active = ($controller == 'UserController') ? 'selected' : '';
                @endphp
                <li class="sidebar-item {{$active}}"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link active" href="{{ route('admin.list-users') }}" aria-expanded="false">
                        <i class="mdi mdi-account-circle"></i>
                        <span class="hide-menu">Users</span>
                    </a>
                </li>
                @php
                    $active = ($controller == 'CategoryController') ? 'selected' : '';
                @endphp
                <li class="sidebar-item {{$active}}"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link active" href="{{ route('admin.list-category') }}" aria-expanded="false">
                        <i class="mdi mdi-account-multiple"></i>
                        <span class="hide-menu">Category</span>
                    </a>
                </li>
                <li class="sidebar-item "> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link active" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" aria-expanded="false">
                        <i class="fa fa-power-off"></i>
                        <span class="hide-menu">Logout</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
<!-- End Sidebar scroll-->
</aside>